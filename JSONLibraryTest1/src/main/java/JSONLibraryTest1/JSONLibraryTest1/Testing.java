package JSONLibraryTest1.JSONLibraryTest1;

import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.Criteria;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;

public class Testing {

	public JSONArray jsonFetch() {

		System.out.println("**************************Json Data***************************");
		JSONParser jsonParser = new JSONParser();
		JSONArray employeeList = null;
		File f = new File("JsonData.json");
//		System.out.println("---------------"+f.getAbsolutePath());
		try (FileReader reader = new FileReader(f.getAbsolutePath())) {
			// Read JSON file
			Object obj = jsonParser.parse(reader);
			employeeList = (JSONArray) obj;
//			System.out.println("---------------------------");
//			System.out.println(employeeList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return employeeList;
	}

//	@Test
	// Display all users name
	public void displayAllUserNames() {
		System.out.println("**************************Display all users name***************************");
		JSONArray json = jsonFetch();
		DocumentContext context = JsonPath.parse(json);
		List<Object> listUsers = context.read("$[*]['name']");
		for (Object user : listUsers) {
			System.out.println(user);
		}
		System.out.println("length - > " + listUsers.size());
	}

//	@Test
	// Display first two users only
	public void displayFirstTwoUsers() {
		System.out.println("**************************Display first two users only***************************");
		JSONArray json = jsonFetch();
		List<Object> listUsers = JsonPath.parse(json).read("$[0,1]['name']");
		for (Object user : listUsers) {
			System.out.println(user);
		}
	}

//	@Test
	// Display last two users only
	public void displayLastTwoUsers() {
		System.out.println("**************************Display last two users only***************************");
		JSONArray json = jsonFetch();
		List<Object> listUsers = JsonPath.parse(json).read("$[-2:]['name']");
		for (Object user : listUsers) {
			System.out.println(user);
		}
	}

//	@Test
	// Display all users who lives in California
	public void displayCaliforniaUsers() {
		System.out.println(
				"**************************Display all users who lives in California***************************");
		JSONArray json = jsonFetch();
//		Filter filter = Filter.filter(Criteria.where("address").regex(Pattern.compile(".*California.*")));
		Filter filter = Filter.filter(Criteria.where("address").regex(Pattern.compile(".*California.*")));
		List<Object> californiaUsers = JsonPath.read(json, "$..[?]", filter);
		for (Object user : californiaUsers) {
			System.out.println("California Data--->" + user);
			System.out.println("california user name ----->" + JsonPath.parse(user).read("$['name']"));
		}
	}

//	@Test
	// Display all female users
	public void displayAllFemaleUsers() {
		System.out.println("**************************Display all female users***************************");
		JSONArray json = jsonFetch();
//		Filter filter = Filter.filter(Criteria.where("address").regex(Pattern.compile(".*California.*")));
		Filter filter = Filter.filter(Criteria.where("gender").regex(Pattern.compile("female")));
		List<Object> californiaUsers = JsonPath.read(json, "$..[?]", filter);
		for (Object user : californiaUsers) {
			System.out.println("Female Data--->" + user);
			System.out.println("Female user name ----->" + JsonPath.parse(user).read("$['name']"));
		}
	}

//	@Test
	// Display total number of friends for first user
	public void firstUserTotalFriends() {
		System.out.println(
				"**************************Display total number of friends for first user***************************");
		JSONArray json = jsonFetch();
		List<Object> users = JsonPath.parse(json).read("$[?(@.age > 24 && @.age < 36)]['name']");
		for (Object user : users) {
			System.out.println("User Data--->" + user);
		}
	}

//	@Test
	// Display balance of male users
	public void displayMaleUserBalance() {
		System.out.println("**************************Display balance of male users***************************");
		JSONArray json = jsonFetch();
		Filter filter = Filter.filter(Criteria.where("gender").regex(Pattern.compile("male")));
		List<Object> californiaUsers = JsonPath.read(json, "$..[?]", filter);
		double totalVal = 0 ;
		for (Object user : californiaUsers) {
			System.out.println("Male User Data--->" + user);
			System.out.println("Male user Balance ----->" + JsonPath.parse(user).read("$['balance']"));
			String val = JsonPath.parse(user).read("$['balance']");
			totalVal += Double.parseDouble(val.replaceAll("[^0-9.]","")); 
		}
		System.out.println("TOtal => "+totalVal);
		Assert.assertEquals(30007.0, totalVal, "total should be 30007.0");
	}
	
	@Test
	// Display total number of friends for first user
	public void displayTotalFriendsForFirstUser() {
		System.out.println(
				"**************************Display total number of friends for first user***************************");
		JSONArray json = jsonFetch();
		DocumentContext context = JsonPath.parse(json);
		int length = context.read("$[0]['friends'].length()");
		System.out.println("User 1 friends length --->" + length);
	}
	
}
